#include "elevator.h"
#include "controller.h"
#include "hardware.h"


void controller_check_floor_buttons(ElevatorState* p_currentState){
	for(int i = 0; i < HARDWARE_NUMBER_OF_FLOORS; i++){
		for(int j= 0; j < HARDWARE_NUMBER_OF_ORDERS; j++){
			if(hardware_read_order(i, j) && !hardware_read_stop_signal()){
				elevator_new_order(i, j, p_currentState);
			}			
		}	
	}
}

void controller_check_stop_button(ElevatorState* p_currentState){
	if(hardware_read_stop_signal()){
		motor_stop(p_currentState);
		hardware_command_stop_light(1);
		elevator_delete_all_orders(p_currentState);
		motor_open_door(p_currentState);
		motor_close_door(p_currentState);
	}
	else
	{	
		hardware_command_stop_light(0);
	}
	
}

int controller_orders_above(ElevatorState* p_currentState){
	for(unsigned int i = HARDWARE_NUMBER_OF_FLOORS - 1; i > elevator_get_current_floor(p_currentState); i--){
		for (unsigned int j = 0; j < HARDWARE_NUMBER_OF_ORDERS; j++){
			if(p_currentState->orderMatrix[i][j]){
				return 1;
			}
		}
	}
	return 0;
}

int controller_orders_below(ElevatorState* p_currentState){
	for(unsigned int i = 0; i < elevator_get_current_floor(p_currentState); i++){
		for (unsigned int j = 0; j < HARDWARE_NUMBER_OF_ORDERS; j++){
			if(p_currentState->orderMatrix[i][j]){
				return 1;
			}
		}
	}
	return 0;
}

int controller_inside_orders_above(ElevatorState* p_currentState){
	for(unsigned int i = HARDWARE_NUMBER_OF_FLOORS -1; i > elevator_get_current_floor(p_currentState); i--){
		if(p_currentState->insideOrder[i]){
			return 1;
		}
	}
	return 0;
}

int controller_inside_orders_below(ElevatorState* p_currentState){
	for(unsigned int i = 0; i < elevator_get_current_floor(p_currentState); i++){
        if(p_currentState->insideOrder[i]){
        	return 1;
		}
    }
	return 0;
}

int controller_inside_orders_here(ElevatorState* p_currentState){
	if(p_currentState->insideOrder[elevator_get_current_floor(p_currentState)]){
		return 1;
	}
	return 0;
}

int controller_between_floors(){
	for(unsigned int i = 0; i < HARDWARE_NUMBER_OF_FLOORS; i++){
		if(hardware_read_floor_sensor(i)){
			return 0;
		}
	}
	return 1;
}

void controller_complete_orders_here(ElevatorState* p_currentState){
	//If stopped and at floor, let people on and off
	if(!controller_between_floors()){
		for(unsigned int i = 0; i < HARDWARE_NUMBER_OF_ORDERS; i++){
			if(elevator_check_order_here(i, p_currentState)){
				motor_stopping_at_floor(elevator_get_current_floor(p_currentState), p_currentState);
				break;
			}
		}
	}
}

void controller_enable_return_if_between_floors(ElevatorState* p_currentState){
	//If stopped and not at a floor, make sure elevator can return to last floor
	if(controller_between_floors()){
		for(unsigned int i = 0; i < HARDWARE_NUMBER_OF_ORDERS; i++){
			if(elevator_check_order_here(i, p_currentState)){
				if(elevator_get_previous_direction(p_currentState) == HARDWARE_MOVEMENT_UP){
					motor_down(p_currentState);
					elevator_set_current_floor(elevator_get_current_floor(p_currentState)+ 1, p_currentState);
				}
				else if(elevator_get_previous_direction(p_currentState) == HARDWARE_MOVEMENT_DOWN){
					motor_up(p_currentState);
					elevator_set_current_floor(elevator_get_current_floor(p_currentState)- 1, p_currentState);
				}
			}
		}
	}
}

void controller_prioritize_orders_in_current_direction(ElevatorState* p_currentState){
	//If direction was upwards, check below and override with above
		if(elevator_get_previous_direction(p_currentState) == HARDWARE_MOVEMENT_UP){
			if(controller_orders_below(p_currentState)){
				motor_down(p_currentState);		
			} 
			if(controller_orders_above(p_currentState)){
				motor_up(p_currentState);
			}
		}
		//If direction was downwards, check above and ovveride with below
		if(elevator_get_previous_direction(p_currentState) == HARDWARE_MOVEMENT_DOWN){
			if(controller_orders_above(p_currentState)){
				motor_up(p_currentState);		
			} 
			if(controller_orders_below(p_currentState)){
				motor_down(p_currentState);
			}
		}
}

void controller_prioritize_inside_orders_above(ElevatorState* p_currentState){
	if(controller_inside_orders_below(p_currentState)){
		motor_down(p_currentState);		
	} 
	if(controller_inside_orders_above(p_currentState)){
		motor_up(p_currentState);
	}
}

void controller_prioritize_inside_orders_below(ElevatorState* p_currentState){
	if(controller_inside_orders_above(p_currentState)){
		motor_up(p_currentState);		
	} 
	if(controller_inside_orders_below(p_currentState)){
		motor_down(p_currentState);
	}
}

void controller_prioritize_from_bottom(ElevatorState* p_currentState){
	for(unsigned int i = 0; i < HARDWARE_NUMBER_OF_FLOORS; i++){
		if(p_currentState->insideOrder[i]){
			if(elevator_get_current_floor(p_currentState) - i > 0){
				motor_up(p_currentState);
			}
			else if(elevator_get_current_floor(p_currentState) - i < 0){
				motor_down(p_currentState);
			}
		}
	}
}

void controller_stop_at_relevant_orders_up(ElevatorState* p_currentState){
	if (controller_inside_orders_above(p_currentState) && elevator_check_order_here(HARDWARE_ORDER_DOWN, p_currentState) && !elevator_check_order_here(HARDWARE_ORDER_UP, p_currentState)){
		if(!controller_inside_orders_here(p_currentState)){
			;
		}
	}
	else if(!controller_inside_orders_here(p_currentState) && !elevator_check_order_here(HARDWARE_ORDER_UP, p_currentState) && controller_orders_above(p_currentState)){
		;
	}
	else{
		motor_stop(p_currentState);
		}
}

void controller_stop_at_relevant_orders_down(ElevatorState* p_currentState){
	if (controller_inside_orders_below(p_currentState) && elevator_check_order_here(HARDWARE_ORDER_UP, p_currentState) && !elevator_check_order_here(HARDWARE_ORDER_DOWN, p_currentState)){
		if(!controller_inside_orders_here(p_currentState)){
			;
		}
	}
	else if(!controller_inside_orders_here(p_currentState) && !elevator_check_order_here(HARDWARE_ORDER_DOWN, p_currentState)&& controller_orders_below(p_currentState)){
		;
	}
	else{
		motor_stop(p_currentState);
	}
}


void controller_drive_elevator(ElevatorState* p_currentState){
	
	controller_check_floor_buttons(p_currentState);
	controller_check_stop_button(p_currentState);

	switch (elevator_get_movement_direction(p_currentState))
	{
    	case HARDWARE_MOVEMENT_STOP: ;

			controller_complete_orders_here(p_currentState);
			controller_enable_return_if_between_floors(p_currentState);

			//If no inside orders:
			if(!controller_inside_orders_above(p_currentState) && !controller_inside_orders_below(p_currentState)){
				controller_prioritize_orders_in_current_direction(p_currentState);
				break;
			}
			//If direction was upwards, check below and override with above
			else if(elevator_get_previous_direction(p_currentState) == HARDWARE_MOVEMENT_UP){
				controller_prioritize_inside_orders_above(p_currentState);
			}
			//If direction was downwards, check above and overide with below
			else if(elevator_get_previous_direction(p_currentState) == HARDWARE_MOVEMENT_DOWN){
				controller_prioritize_inside_orders_below(p_currentState);
			}
			else{
			//Default in case previous direction not found
				controller_prioritize_from_bottom(p_currentState);
			}
			break;
		case HARDWARE_MOVEMENT_UP: ;
			//When we hit a floor:
			if(hardware_read_floor_sensor(elevator_get_current_floor(p_currentState) + 1)){
				elevator_set_current_floor(elevator_get_current_floor(p_currentState) +1, p_currentState);
				controller_stop_at_relevant_orders_up(p_currentState);
			}
        	break;
    	case HARDWARE_MOVEMENT_DOWN: ;
			//When we hit a floor:
			if(hardware_read_floor_sensor(elevator_get_current_floor(p_currentState) - 1)){
				elevator_set_current_floor(elevator_get_current_floor(p_currentState) -1, p_currentState);
				controller_stop_at_relevant_orders_down(p_currentState);
			}
			break;
	}	
}
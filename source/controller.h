/**
 * @file
 * @brief Control logic for elevators state machine
 */

#pragma once
#include "elevator.h"
#include "motor.h"

/**
* @brief checks if floor buttons is pressed, and set a new order
* @param p_currentState reference to elevators current state
*/
void controller_check_floor_buttons(ElevatorState* p_currentState);

/**
* @brief cheacks if stop button is pressed, and call functions og what should happend when it is pressed.
* @param p_currentState reference to elevators current state
*/
void controller_check_stop_button(ElevatorState* p_currentState);

/**
 * @brief used to check if orders below
 * @param p_currentState pointer to current state
 * @return return 1 if orders below, 0 if not
 */
int controller_orders_below(ElevatorState* p_currentState);

/**
 * @brief used to check if orders above
 * @param p_currentState pointer to current state
 * @return return 1 if orders above, 0 if not
 */
int controller_orders_above(ElevatorState* p_currentState);

/**
 * @brief used to check if inside orders above
 * @param p_currentState pointer to current state
 * @return return 1 if inside orders above, 0 if not
 */
int controller_inside_orders_above(ElevatorState* p_currentState);

/**
 * @brief used to check if inside orders below
 * @param p_currentState pointer to current state
 * @return return 1 if  inside orders below, 0 if not
 */
int controller_inside_orders_below(ElevatorState* p_currentState);

/**
 * @brief used to check if inside orders here
 * @param p_currentState pointer to current state
 * @return return 1 if inside orders here, 0 if not
 */
int controller_inside_orders_here(ElevatorState* p_currentState);

/**
 * @brief used to check if between floors
 * @return return 1 if between floors, 0 if not
 */
int controller_between_floors();

/**
 * @brief Function that terminates eventual orders at floor. Contains safety checks to ensure we are at a floor. Does nothing if there are no orders here
 * @param p_currentState pointer to current state
 */

void controller_complete_orders_here(ElevatorState* p_currentState);

/**
 * @brief Function that enables the elevator to return to it's previous floor if abruptly stopped between floors. Uses previous direction to identify between which two floors we are
 * @param p_currentState pointer to current state
 */

void controller_enable_return_if_between_floors(ElevatorState* p_currentState);

/**
 * @brief Function that sets movement direction according to whether there are orders in the direction we were currently moving. Does not take inside orders into consideration
 * @param p_currentState pointer to current state
 */

void controller_prioritize_orders_in_current_direction(ElevatorState* p_currentState);

/**
 * @brief Function that prioritizes inside orders above current state when deciding movement direction
 * @param p_currentState pointer to current state
 */

void controller_prioritize_inside_orders_above(ElevatorState* p_currentState);

/**
 * @brief Function that prioritizes inside orders below current state when deciding movement direction
 * @param p_currentState pointer to current state
 */

void controller_prioritize_inside_orders_below(ElevatorState* p_currentState);

/**
 * @brief Function that prioritizes orders from the bottom. This is a default that is only to be used if none of the other prioritizers can be used. Bottom is preffered as it is harder to get up stairs than down.
 * @param p_currentState pointer to current state
 */

void controller_prioritize_from_bottom(ElevatorState* p_currentState);

/**
 * @brief Function that stops the elevator if there are relevant orders at this floor. Relevant as according to movement direction upwards
 * @param p_currentState pointer to current state
 */

void controller_stop_at_relevant_orders_up(ElevatorState* p_currentState);

/**
 * @brief Function that stops the elevator if there are relevant orders at this floor. Relevant as according to movement direction downwards
 * @param p_currentState pointer to current state
 */

void controller_stop_at_relevant_orders_down(ElevatorState* p_currentState);

/**
 * @brief Main driver function that decides which direction to head in based on current state.
 * @param p_currentState pointer to current state
 */

void controller_drive_elevator(ElevatorState* p_currentState);


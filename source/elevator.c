#include "elevator.h"

void elevator_new_order(int floor, HardwareOrder orderType, ElevatorState* p_currentState){
	p_currentState->orderMatrix[floor][(int)orderType] = 1;
	if(orderType == HARDWARE_ORDER_INSIDE){
		p_currentState->insideOrder[floor] = 1;
	}
	hardware_command_order_light(floor, orderType, 1);	
}

void elevator_delete_orders_at_floor(int floor, ElevatorState* p_currentState){
		p_currentState->insideOrder[floor] = 0;
		for(int j = 0; j < HARDWARE_NUMBER_OF_ORDERS; j++){
			p_currentState->orderMatrix[floor][j] = 0;
			hardware_command_order_light(floor, j, 0);
		}
}

void elevator_delete_all_orders(ElevatorState* p_currentState){
	for(int i = 0; i < HARDWARE_NUMBER_OF_FLOORS; i++){
		p_currentState->insideOrder[i] = 0;
		for(int j = 0; j < HARDWARE_NUMBER_OF_ORDERS; j++){
			p_currentState->orderMatrix[i][j] = 0;
			hardware_command_order_light(i, j, 0);
		}
	}
}

void elevator_set_current_floor(int floor, ElevatorState* p_currentState){
	p_currentState->currentFloor = floor;
	hardware_command_floor_indicator_on(floor);
}

int elevator_get_current_floor(ElevatorState* p_currentState){
	return(p_currentState->currentFloor);
}

void elevator_set_movement_direction(HardwareMovement dir, ElevatorState* p_currentState){
	if(p_currentState->direction != HARDWARE_MOVEMENT_STOP){
		p_currentState->lastDirection = p_currentState->direction;
	}
	p_currentState->direction = dir;
}

HardwareMovement elevator_get_movement_direction(ElevatorState* p_currentState){
	return(p_currentState->direction);
}

HardwareMovement elevator_get_previous_direction(ElevatorState* p_currentState){
	return(p_currentState->lastDirection);
}

int elevator_check_order_here(HardwareOrder orderType, ElevatorState* p_currentState){
	return p_currentState->orderMatrix[elevator_get_current_floor(p_currentState)][orderType];
}
/**
 * @file
 * @brief State machine for the elevator with all required functions
 */

#pragma once
#include "hardware.h"

/**
*@brief Struct to define the elevators current state
*/
typedef struct{
	int currentFloor; /**< Variable that contains the last known visited floor */
	HardwareMovement direction; /**< Variable that contains the current direction (including STOP)*/
	HardwareMovement lastDirection; /**< Variable that contains the last known moving direction */
	int orderMatrix[4][3]; /**< A four by three matrix that keeps track of which order types can be found at the different floors */
	int insideOrder[4]; /**< A standalone matrix to keep track of which floors currently have an inside order. Makes prioritizing inside orders easier for the controller */
}ElevatorState; 

/**
 * @brief Function for placing new orders and setting corresponding lights
 * @param floor floor for requesting order to be placed
 * @param orderType which type of order is being placed on floor in question
 * @param p_currentState reference to elevators current state
*/
void elevator_new_order(int floor, HardwareOrder orderType, ElevatorState* p_currentState);


/**
 * @brief Function for deleting all orders (and lights) at given floor
 * @param p_currentState points to location of current state
 * @param floor floor we want to delete orders at
 */

void elevator_delete_orders_at_floor(int floor, ElevatorState* p_currentState);

/**
 * @brief Function for deleting all orders when stop button is pressed
 * @param p_currentState points to the order matrix we want to reset
 */
void elevator_delete_all_orders(ElevatorState* p_currentState);

/**
 * @brief Function that sets the light and variable for current floor
 * @param floor defines which floor we're at
 * @param p_currentState reference to which state to update
 */

void elevator_set_current_floor(int floor, ElevatorState* p_currentState);

/**
 * @brief Function that gets the 'current' floor meaning current or last.
 * @param p_currentState reference to state
 * @warning current floor meaning last integer floor visited
 * @return current floor as a 0-indexed integer
 */

int elevator_get_current_floor(ElevatorState* p_currentState);

/**
 * @brief sets the movement variable in a referenced elevator state
 * @warning DOES NOT SET THE ACTUAL MOTOR VOLTAGE, ONLY THE VARIABLE
 * @param dir enum HardwareMovement as defined in hardware.h
 * @param p_currentState reference to state which is being updated
 */

void elevator_set_movement_direction(HardwareMovement dir, ElevatorState* p_currentState);

/**
 * @brief returns last known movement direction from referenced elevator state
 * @param p_currentState referenced elevator state to get info from
 * @return HardwareMovement enum as defined in hardware.h gives us last 'direction'
 */

HardwareMovement elevator_get_movement_direction(ElevatorState* p_currentState);

/**
 * @brief Function to return the previous movement direction
 * @param p_currentState referenced elevator state
 * @return HardwareMovement that is previous direction.
 */

HardwareMovement elevator_get_previous_direction(ElevatorState* p_currentState);

/**
 * @brief Function to check if order exists at current floor, of type orderType
 * @param orderType orderType to check
 * @param p_currentState pointer to elevators statein which we check for orders
 */

int elevator_check_order_here(HardwareOrder orderType, ElevatorState* p_currentState);
#include <stdio.h>
#include <stdlib.h>

#include "startup.h"
#include "controller.h"


int main(){


    printf("=== Welcome to Magne & Tobias' elevator ===\n");
    printf("Use the control panel to move the elevator\n");
    
    ElevatorState* p_state = startup();
    while(1){
        controller_drive_elevator(p_state);
    }
    //Since the elevator is designed to run for eternety, the following is commented out:
    //free(p_state);

    return 0;
}

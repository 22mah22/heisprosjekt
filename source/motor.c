
#include "motor.h"
#include "hardware.h"
#include "elevator.h"
#include "pause.h"

int motor_on_floor(){
	for (int i = 0; i < HARDWARE_NUMBER_OF_FLOORS; i++){
		if(hardware_read_floor_sensor(i)){
			return 1;
		} 
	}
    return 0;
}

void motor_stopping_at_floor(int floor, ElevatorState* p_currentState){
    motor_open_door(p_currentState);
    //Close door includes a 3 second wait
    motor_close_door(p_currentState);
    elevator_delete_orders_at_floor(floor, p_currentState);
    
}

void motor_close_door(ElevatorState* p_currentState){
    if(motor_on_floor()){
        pause_three_second_wait(p_currentState);
        if(!hardware_read_obstruction_signal() && !hardware_read_stop_signal()){
            hardware_command_door_open(0);
        }
    }
}

//If still and at a floor, 
void motor_open_door(ElevatorState* p_currentState){
    if(motor_on_floor() && (elevator_get_movement_direction(p_currentState) == HARDWARE_MOVEMENT_STOP)){ 
         hardware_command_door_open(1); 
    } 
}

void motor_stop(ElevatorState* p_currentState){
   	elevator_set_movement_direction(HARDWARE_MOVEMENT_STOP, p_currentState);
	hardware_command_movement(elevator_get_movement_direction(p_currentState));
}

void motor_up(ElevatorState* p_currentState){
        elevator_set_movement_direction(HARDWARE_MOVEMENT_UP, p_currentState);
        hardware_command_movement(elevator_get_movement_direction(p_currentState));
}

void motor_down(ElevatorState* p_currentState){
        elevator_set_movement_direction(HARDWARE_MOVEMENT_DOWN, p_currentState);
        hardware_command_movement(elevator_get_movement_direction(p_currentState));
}


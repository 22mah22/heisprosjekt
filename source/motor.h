/**
 * @file
 * @brief Interface for controlling the elevator and door motors
 */

#pragma once
#include "elevator.h"

/**
* @brief check if the elevator is on a floor or in between
* @return 1 if on floor 0 otherwise
*/
int motor_on_floor();

/**
 * @brief Funcion for deleting orders and lights at a floor the elevator stops at.
 * @param floor where the elevator is currently stopping
 * @param p_currentState reference to current state including active orders
 */
void motor_stopping_at_floor(int floor, ElevatorState* p_currentState);

/**
* @brief closes the door if conditions are met
* @param p_currentState reference to elevators current state
*/
void motor_close_door(ElevatorState* p_currentState);

/**
* @brief opens the door if contitions are met
* @param p_currentState reference to elevators current state 
*/
void motor_open_door(ElevatorState* p_currentState);

/**
* @brief stops the motor
* @param p_currentState reference to elevators current state 
*/
void motor_stop(ElevatorState* p_currentState);

/**
* @brief moves the elevator up
* @param p_currentState reference to elevators current state 
*/
void motor_up(ElevatorState* p_currentState);

/**
* @brief moves the elevator down
* @param p_currentState reference to elevators current state 
*/
void motor_down(ElevatorState* p_currentState);



#include <time.h>
#include "pause.h"
#include "elevator.h"
#include "controller.h"


void pause_three_second_wait (ElevatorState* p_currentState){
	clock_t x_countTime, x_startTime;
	unsigned int count_down_time = 3;
	int time_left = 0;

	x_startTime = clock();
	time_left = count_down_time;
	hardware_command_stop_light(0);

	while(time_left > 0){
		x_countTime = clock();
		time_left = count_down_time - ((x_countTime - x_startTime)/(CLOCKS_PER_SEC));
		if(hardware_read_obstruction_signal()){
			x_startTime = clock();
		}
		if(hardware_read_stop_signal()){
			x_startTime = clock();
			elevator_delete_all_orders(p_currentState);
			hardware_command_stop_light(1);
		}
		else{
			hardware_command_stop_light(0);
			controller_check_floor_buttons(p_currentState);
		}
	}
}

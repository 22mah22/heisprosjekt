/**
 * @file
 * @brief Module for implementing a three second wait time
 */

#pragma once
#include "elevator.h"
#include <time.h>

/**
 *@brief function that handles the 3 second wait time for doors to open. Listens for orders, stop and obstruct while d *oors are open.
 *@param p_currentstate pointer to elevators current state
 */ 
void pause_three_second_wait(ElevatorState* p_currentstate);

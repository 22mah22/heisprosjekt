#include <stdlib.h>
#include <stdio.h>
#include "elevator.h"
#include "startup.h"

void clear_all_order_lights(){
    HardwareOrder order_types[3] = { 
        HARDWARE_ORDER_UP,
        HARDWARE_ORDER_INSIDE,
        HARDWARE_ORDER_DOWN
    }; /**< Enum that differentiates the different types of order lights */

    for(int f = 0; f < HARDWARE_NUMBER_OF_FLOORS; f++){
        for(int i = 0; i < 3; i++){
            HardwareOrder type = order_types[i];
            hardware_command_order_light(f, type, 0);
        }
    }
}

ElevatorState* startup(void){

	int error = hardware_init();
    if(error != 0){
        fprintf(stderr, "Unable to initialize hardware\n");
        exit(1);
    }
	clear_all_order_lights();
	
	//if not at bottom, go to bottom
	while(!hardware_read_floor_sensor(0)){
		hardware_command_movement(HARDWARE_MOVEMENT_DOWN);
	}
	//stop at bottom and initiate still state elevator at bottom
	hardware_command_movement(HARDWARE_MOVEMENT_STOP);
	ElevatorState *p_currentState;
	p_currentState = (ElevatorState *) malloc(sizeof(ElevatorState));
	elevator_set_current_floor(0, p_currentState);
	p_currentState->direction = HARDWARE_MOVEMENT_STOP;
	p_currentState->lastDirection = HARDWARE_MOVEMENT_DOWN;
	for(unsigned int i = 0; i < HARDWARE_NUMBER_OF_FLOORS; i++){
		p_currentState->insideOrder[i] = 0;
		for(unsigned int j = 0; j < HARDWARE_NUMBER_OF_ORDERS; j++){
			p_currentState->orderMatrix[i][j] = 0;
		}
	}
	return p_currentState;
}

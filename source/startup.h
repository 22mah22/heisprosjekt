/**
 * @file
 * @brief Module for defining the elevators initial state at program startup
 */

#pragma once
#include "elevator.h"


/**
 * @brief Function to turn off all eventual lights on the panel
 */

void clear_all_order_lights();

/**
 * @brief Function to intitate a state at program startup
 * @return elevatorState* returns a pointer to newly allocated struct in memory
 */

ElevatorState* startup(void);
